import numpy as np
import math
import sys
import time
sys.setrecursionlimit(5000)

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Disk:
    def __init__(self, center, radius):
        self.center = center
        self.radius = radius

def circle_from_points(p1, p2, p3):
    # Calcule le cercle circonscrit à trois points
    ax, ay = p1.x, p1.y
    bx, by = p2.x, p2.y
    cx, cy = p3.x, p3.y

    d = 2 * (ax * (by - cy) + bx * (cy - ay) + cx * (ay - by))
    if d == 0:
        return None  # Les points sont alignés

    ux = ((ax ** 2 + ay ** 2) * (by - cy) + (bx ** 2 + by ** 2) * (cy - ay) + (cx ** 2 + cy ** 2) * (ay - by)) / d
    uy = ((ax ** 2 + ay ** 2) * (cx - bx) + (bx ** 2 + by ** 2) * (ax - cx) + (cx ** 2 + cy ** 2) * (bx - ax)) / d

    radius = math.sqrt((ax - ux) ** 2 + (ay - uy) ** 2)
    center = Point(ux, uy)
    return Disk(center, radius)

def welzl(points):
    boundary_points = []
    
    while len(points) > 0:
        p = np.random.choice(points)
        points.remove(p)
        
        if len(boundary_points) < 3:
            boundary_points.append(p)
        elif len(boundary_points) == 3:
            disk = circle_from_points(boundary_points[0], boundary_points[1], boundary_points[2])
            if disk is not None and math.sqrt((p.x - disk.center.x) ** 2 + (p.y - disk.center.y) ** 2) <= disk.radius:
                continue
            else:
                boundary_points.append(p)
                boundary_points.pop(0)
        
    if len(boundary_points) == 0:
        return Disk(Point(0, 0), 0)
    elif len(boundary_points) == 1:
        return Disk(boundary_points[0], 0)
    elif len(boundary_points) == 2:
        radius = math.sqrt((boundary_points[0].x - boundary_points[1].x) ** 2 + (boundary_points[0].y - boundary_points[1].y) ** 2) / 2
        center = Point((boundary_points[0].x + boundary_points[1].x) / 2, (boundary_points[0].y + boundary_points[1].y) / 2)
        return Disk(center, radius)
    else:
        return circle_from_points(boundary_points[0], boundary_points[1], boundary_points[2])

def read_points_from_file(filename):
    # Open the file for reading
    with open(filename, "r") as file:
        # Initialize an empty list to store the coordinate pairs
        points = []
        
        # Read each line in the file
        for line in file:
            # Split the line into x and y values
            x, y = map(int, line.strip().split())
            p = Point(x, y)
            points.append(p)
    return points

def read_all_points_from_files():
    all_points = []
    for i in range(2,5):
        filename = r'samples\test-' + str(i) + '.points'
        points = read_points_from_file(filename)
        all_points.extend(points)
    return all_points

def read_points_from_file(filename):
    # Open the file for reading
    with open(filename, "r") as file:
        # Initialize an empty list to store the coordinate pairs
        points = []
        
        # Read each line in the file
        for line in file:
            # Split the line into x and y values
            x, y = map(int, line.strip().split())
            p = Point(x, y)
            points.append(p)
    return points

def valuetoreturnWezl():
    points = read_all_points_from_files()
    timestart = time.time()
    result = welzl(points)
    timestop = time.time()
    execution_time=timestop-timestart
    print("Resultat de l'algo WELZL : ")
    print("Centre:", result.center.x, result.center.y)
    print("Rayon:", result.radius)
    print("Execution time of algo WELZL", ":", execution_time, "seconds")
    return execution_time

valuetoreturnWezl()

def tabOfExecution_time_welzl_Comparaison(filestart,fileend):
    tabOfExecution_time = []  # Initialize the list outside the loop
    for i in range(filestart,fileend):
        filename = r'samples\test-' + str(i) + '.points'
        points = read_points_from_file(filename)
        timestart = time.time()
        result = welzl(points)
        timestop = time.time()
        execution_time = timestop - timestart
        tabOfExecution_time.append(execution_time)
        somme = sum(tabOfExecution_time)
        print("Ceci est la somme de temps d'exécution sur l'ensemble des fichier de ",filestart,"à",fileend ,tabOfExecution_time_welzl_Comparaison(2,10))
    return somme

def tabOfExecution_time_welzl():
    tabOfExecution_time = []  # Initialize the list outside the loop
    for i in range(2, 5):
        filename = r'samples\test-' + str(i) + '.points'
        points = read_points_from_file(filename)
        timestart = time.time()
        result = welzl(points)
        timestop = time.time()
        execution_time = timestop - timestart
        tabOfExecution_time.append(execution_time)
    return tabOfExecution_time


tabOfExecution_time_welzl()
print(tabOfExecution_time_welzl())
tabOfExecution_time_welzl_Comparaison(2,10)



