import math
import time
# Classe pour représenter un point
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

# Fonction pour calculer la distance entre deux points
def distance(point1, point2):
    return math.sqrt((point2.x - point1.x) ** 2 + (point2.y - point1.y) ** 2)

# Fonction pour vérifier si un point est à l'intérieur du cercle
def est_dans_cercle(point, cercle):
    centre, rayon = cercle
    return distance(centre, point) <= rayon

# Fonction pour calculer le cercle minimum couvrant
# Fonction pour calculer le cercle minimum couvrant
def cercle_minimum_couvrant(points):
    n = len(points)

    # Cas de base
    if n == 0:
        return None, float('inf')
    elif n == 1:
        return points[0], 0

    # Parcours de tous les points pour trouver le cercle minimum couvrant
    resultat = None, float('inf')

    for p in points:
        for q in points:
            c = Point((p.x + q.x) / 2, (p.y + q.y) / 2), distance(p, q) / 2

            if all(est_dans_cercle(r, c) for r in points):
                if c[1] < resultat[1]:
                    resultat = c

    for p in points:
        for q in points:
            for r in points:
                if p.x == q.x == r.x or p.y == q.y == r.y:
                    continue

                c = cercle_circonscrit(p, q, r)

                if c is not None and all(est_dans_cercle(point, c) for point in points):
                    if c[1] < resultat[1]:
                        resultat = c

    return resultat

# Fonction pour calculer le cercle circonscrit à trois points
def cercle_circonscrit(point1, point2, point3):
    x1, y1 = point1.x, point1.y
    x2, y2 = point2.x, point2.y
    x3, y3 = point3.x, point3.y
    d = 2 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2))
    if d == 0:
        return None
    ux = ((x1 ** 2 + y1 ** 2) * (y2 - y3) + (x2 ** 2 + y2 ** 2) * (y3 - y1) + (x3 ** 2 + y3 ** 2) * (y1 - y2)) / d
    uy = ((x1 ** 2 + y1 ** 2) * (x3 - x2) + (x2 ** 2 + y2 ** 2) * (x1 - x3) + (x3 ** 2 + y3 ** 2) * (x2 - x1)) / d
    centre = Point(ux, uy)
    rayon = distance(centre, point1)

    return centre, rayon

def read_all_points_from_files():
    all_points = []
    for i in range(2,5):
        filename = r'samples\test-' + str(i) + '.points'
        points = read_points_from_file(filename)
        all_points.extend(points)
    return all_points

def read_points_from_file(filename):
    # Open the file for reading
    with open(filename, "r") as file:
        # Initialize an empty list to store the coordinate pairs
        points = []
        
        # Read each line in the file
        for line in file:
            # Split the line into x and y values
            x, y = map(int, line.strip().split())
            p = Point(x, y)
            points.append(p)
    return points

def valuetoreturnAlgonaif():
    points = read_all_points_from_files()
    timestart = time.time()
    # Calcul du cercle minimum couvrant
    result = cercle_minimum_couvrant(points)
    timestop = time.time()
    # Affichage des résultats
    print("Résultat de l'algorithme naif:")
    print("Centre :", result[0].x, result[0].y)
    print("Rayon :", result[1])
    execution_time=timestop-timestart
    print("Temps d'exécution", ":", execution_time, "seconds")
    return execution_time

valuetoreturnAlgonaif()
def tabOfExecution_time_Algonaif_Comparaison():
    tabOfExecution_time = []  # Initialize the list outside the loop
    for i in range(2, 10):
        filename = r'samples\test-' + str(i) + '.points'
        points = read_points_from_file(filename)
        timestart = time.time()
        result = cercle_minimum_couvrant(points)
        timestop = time.time()
        execution_time = timestop - timestart
        tabOfExecution_time.append(execution_time)
    return tabOfExecution_time

def tabOfExecution_time_Algonaif():
    tabOfExecution_time = []  # Initialize the list outside the loop
    for i in range(2, 5):
        print(i)
        filename = r'samples\test-' + str(i) + '.points'
        points = read_points_from_file(filename)
        timestart = time.time()
        result = cercle_minimum_couvrant(points)
        timestop = time.time()
        execution_time = timestop - timestart
        tabOfExecution_time.append(execution_time)
    return tabOfExecution_time

tabOfExecution_time_Algonaif()
print(tabOfExecution_time_Algonaif())



