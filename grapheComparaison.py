import matplotlib.pyplot as plt
import algonaif
import welzl

# Temps d'exécution en secondes pour chaque algorithme
temps_algo1 = [welzl.tabOfExecution_time_Algonaif_Comparaison(2,5),welzl.tabOfExecution_time_welzl_Comparaison(2,10),welzl.tabOfExecution_time_welzl(2,20)]
temps_algo2 = [algonaif.tabOfExecution_time_Algonaif_Comparaison(2,5),algonaif.tabOfExecution_time_Algonaif_Comparaison(2,10),algonaif.tabOfExecution_time_Algonaif(2,20)]

# Tâches ou processus correspondants
taches = [768, 2560, 4608]

# Largeur des barres
largeur_barre = 0.35

# Création de l'axe x pour chaque algorithme avec un décalage pour les barres
x_algo1 = range(len(taches))
x_algo2 = [x + largeur_barre for x in x_algo1]

# Création du graphique à barres
plt.bar(x_algo1, temps_algo1, width=largeur_barre, label='Algo 1')
plt.bar(x_algo2, temps_algo2, width=largeur_barre, label='Algo 2')

# Ajout des labels d'axe et de la légende
plt.xlabel('Tâches')
plt.ylabel('Temps d\'exécution (secondes)')
plt.title('Temps d\'exécution des algorithmes')
plt.xticks([x + largeur_barre / 2 for x in x_algo1], taches)
plt.legend()

# Ajustement de l'échelle de l'axe y pour mieux visualiser les barres bleues
plt.ylim(0, max(temps_algo2) * 1.2)

# Affichage du graphique
plt.show()
