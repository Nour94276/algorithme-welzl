import matplotlib.pyplot as plt
import numpy as np
import algonaif
import welzl

# Temps d'exécution en secondes pour chaque algorithme (échantillon de mesures)
temps_welzl = welzl.tabOfExecution_time_welzl()
temps_naif =  algonaif.tabOfExecution_time_Algonaif()

# Calcul de la moyenne et de l'écart-type
moyenne_welzl = np.mean(temps_welzl)
moyenne_naif = np.mean(temps_naif)
std_welzl = np.std(temps_welzl)
std_naif = np.std(temps_naif)

# Barres pour le gain en temps (différence des moyennes)
gain_temps = [moyenne_naif - moyenne_welzl]
std_gain_temps = [np.sqrt(std_naif**2 + std_welzl**2)]  # Propagation de l'incertitude

# Création du graphique à barres
plt.bar(0, gain_temps, yerr=std_gain_temps, width=0.5)

# Ajout des labels d'axe et de la légende
plt.xlabel('Algorithme')
plt.ylabel('Gain en temps (secondes)')
plt.title('Gain en temps de Welzl par rapport à l\'algorithme naïf')
plt.xticks([0], ['Welzl vs Naïf'])

# Affichage du graphique
plt.show()
